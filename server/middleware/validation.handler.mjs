const validation_handler = (schema) => {
  return (req, res, next) => {
    const { error } = schema.validate(req.body);

    if (error == null) next();
    else {
      const message = error.details.map((err) => err.message).join(",");
      res.status(422).json({ error: message });
    }
  };
};

export default validation_handler;
