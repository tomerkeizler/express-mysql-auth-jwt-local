import bcrypt from "bcryptjs";
import express from "express";
import log from "@ajar/marker";
import raw from "../../middleware/route.async.wrapper.mjs";
import db from "../../db/mysql.connection.mjs";
import {
  optional_user_schema,
  required_user_schema,
} from "../user/user.schema.mjs";
import validation_handler from "../../middleware/validation.handler.mjs";
import {
  verify_token,
  false_response,
  tokenize,
} from "../../middleware/auth.middleware.mjs";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// REGISTER
router.post(
  "/register",
  validation_handler(required_user_schema),
  raw(async (req, res) => {
    log.obj(req.body, "register, req.body:");

    const sql_search_email = `SELECT id FROM users WHERE email = '${req.body.email}'`;
    const [existingUsers] = await db.query(sql_search_email);
    console.log("fdsfdsfdsfds", existingUsers);

    if (existingUsers.length > 0)
      throw new Error("user already exists with this email address");
      
    // ------------

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    log.info("hashedPassword:", hashedPassword);
    const user_data = {
      ...req.body,
      password: hashedPassword,
    };

    // create a user
    const sql_add_user = `INSERT INTO users SET ?`;
    const [result] = await db.query(sql_add_user, user_data);

    if (!result.affectedRows) {
      return res.status(404).json({ status: `Error while registering` });
    }

    const sql_get_user = `SELECT id FROM users WHERE email = '${user_data.email}'`;
    const [users] = await db.query(sql_get_user);

    if (users.length === 0) {
      return res
        .status(404)
        .json({ status: `User ${req.params.id} was not found` });
    }

    const created_user = users[0];
    // create a token
    const token = tokenize(created_user.id);
    log.info("token:", token);

    return res.status(200).json({
      auth: true,
      token,
      user: created_user,
    });
  })
);

// LOGIN
router.post(
  "/login",
  raw(async (req, res) => {
    // extract from req.body the credentials the user entered
    const { email, password } = req.body;

    // look for the user in db by email
    const sql_get_user = `SELECT id, password FROM users WHERE email = '${email}'`;
    const [result] = await db.query(sql_get_user);

    // if no user found...
    if (result.length === 0) {
      return res
        .status(401)
        .json({ ...false_response, message: "wrong email or password" });
    }

    const user = result[0];
    // check if the password is valid
    const password_is_valid = await bcrypt.compare(password, user.password);
    if (!password_is_valid)
      return res
        .status(401)
        .json({ ...false_response, message: "wrong email or password" });

    // if user is found and password is valid
    // create a fresh new token
    const token = tokenize(user.id);

    // return the information including token as JSON
    return res.status(200).json({
      auth: true,
      token,
    });
  })
);

// LOGOUT
router.get(
  "/logout",
  raw(async (req, res) => {
    return res.status(200).json(false_response);
  })
);

// ME - a protected route
router.get(
  "/me",
  verify_token,
  raw(async (req, res) => {
    const sql = `SELECT id FROM users WHERE id = ${req.user_id}`;
    const [result] = await db.query(sql);

    if (result.length === 0) {
      return res
        .status(404)
        .json({ message: `User ${req.user_id} was not found` });
    }

    res.status(200).json(result[0]);
  })
);

export default router;
